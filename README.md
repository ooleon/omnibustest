# Projeto
[![pipeline status](https://gitlab.com/ooleon/agibank-qatest/badges/master/pipeline.svg)](https://gitlab.com/ooleon/agibank-qatest/commits/master)
[![coverage report](https://gitlab.com/ooleon/agibank-qatest/badges/master/coverage.svg)](https://gitlab.com/ooleon/agibank-qatest/commits/master)

Projeto de exemplo com Spring Boot Java JDK8 para mostrar uso de TDD, BDD, Test unitarios, Test de exception, com algumas tecnologias como Servicios Rest, JPA, h2, elastic search,


DESENVOLVIMENTO EXTRA:
	Alimentar para teste com DataProvider, sendo no mínimo 10 dados para cada;
Configurar build personalizado para rodar testes (por exemplo: tasks específicas no Gradle);
	Usar BDD;
	Gerar Relatório dos testes;

DESENVOLVIMENTO IDEAL:
	Criar sua própria API;

## Tecnologias

As tecnologias usadas são:

- [JUnit4]
- [Maven]
- [SpringBoot] 2.1.6
- [Cucumber] 4.7.1
- [Java] 1.8

### Instalacao / Run

* Git Clone from GitLab
* mvn -q test

>Cada vez que tiver uma mudança no codigo gitlab rodara o Testes.

### Relatorio
cucumber-report.json

# Autor Leon Osty [Linkedin]

[Maven]: <https://maven.apache.org/>
[JUnit4]: <https://junit.org/junit4/>
[SpringBoot]: <https://spring.io/blog/2019/06/19/spring-boot-2-1-6-released>
[Linkedin]: <https://www.linkedin.com/in/ostyleon/>
[Java]: <https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html>
[Cucumber]: <https://cucumber.io/>
