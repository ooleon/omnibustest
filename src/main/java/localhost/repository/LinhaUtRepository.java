package localhost.repository;

import org.springframework.data.repository.CrudRepository;

import localhost.entity.Linhas;

public interface LinhaUtRepository extends CrudRepository<Linhas, String> {

}
