package localhost.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import localhost.entity.Linhas;

public interface LinhasRepository extends JpaRepository<Linhas, Long> {

}
