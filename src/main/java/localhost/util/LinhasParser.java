package localhost.util;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.boot.json.*;

import localhost.entity.Linhas;
import localhost.injection.LinhasService;



/*import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
*/import org.springframework.web.client.RestClientException;



//@RunWith(BlockJUnit4ClassRunner.class)
//@RunWith(SpringRunner.class)
//@SpringBootTest
@RestController
public class LinhasParser {
	// @Autowired
	// private MockMvc mockMvc;
	
	// @MockBean
	
	@Autowired
	private LinhasService linhasService;
	 
//	@Autowired
//	private Environment environment;

	private List<Linhas> listLinhas;
	

//	String exemploLinhasJson = "{ \"id\":\"114633\", \"codigo\":\"60.21-2\", \"nome\":\"HOSPITAL CONCEICAO-IGUATEMI\"}";
	

	public void listJsonLinhas() {
		// bien

		
		RestTemplate restTemplate = new RestTemplate();
		String resp = restTemplate.getForObject(AppConst.LINHAS_URL, String.class);

		JsonParser springParser = JsonParserFactory.getJsonParser();
		// JsonParser springParser = new BasicJsonParser();

		List<Object> list = springParser.parseList(resp);
		List<Linhas> listlnh = new ArrayList<Linhas>();

		for (Object o : list) {
			if (o instanceof Map) {
				Map<String, Object> map = (Map<String, Object>) o;
				System.out.println("Items found: " + map.size());
				Linhas l = new Linhas();
				l.setId(Long.valueOf(map.get("id").toString()));
				l.setCodigo(map.get("codigo").toString());
				l.setNome(map.get("nome").toString());
				
				listlnh.add(l);

				/*
				 * int i = 0; for (Map.Entry<String, Object> entry : map.entrySet()) {
				 * System.out.println(entry.getKey() + " = " + entry.getValue()); i++; }
				 */

			}
		}
		System.out.println(listlnh);
	}

	public void listJsonObj() {
		// bien

		String url = "http://www.poatransporte.com.br/php/facades/process.php?a=nc&p=%&t=l";
		RestTemplate restTemplate = new RestTemplate();
		String resp = restTemplate.getForObject(url, String.class);

		JsonParser springParser = JsonParserFactory.getJsonParser();

		List<Object> list = springParser.parseList(resp);

		for (Object o : list) {
			if (o instanceof Map) {
				Map<String, Object> map = (Map<String, Object>) o;
				System.out.println("Items found: " + map.size());

				int i = 0;
				for (Map.Entry<String, Object> entry : map.entrySet()) {
					System.out.println(entry.getKey() + " = " + entry.getValue());
					i++;
				}

			}
		}
	}





	
	public void getLinhasWeb() {
	}

	
	public void getLinhasParse() {
		try {
			
			InputStream is = AppConst.jsonParseDeUrl(AppConst.LINHAS_URL,AppConst.LINHAS_RECUSO);
//			inputStream = jsonParseDeUrl(urlLinhas,recursoJsonFile); 
			
			// read JSON and load json
			System.out.println("ant");
			ObjectMapper mapper = new ObjectMapper();
			TypeReference<List<Linhas>> typeReference = new TypeReference<List<Linhas>>() {};
			
			// inputStream = TypeReference.class.getResourceAsStream(jsonString);
			//inputStream = new ByteArrayInputStream(jsonString.getBytes(StandardCharsets.UTF_8));

			System.out.println("des");

			// InputStream inputStream =
			// TypeReference.class.getResourceAsStream(jsonString);

			listLinhas = mapper.readValue(is, typeReference);
			// Linhas linha = mapper.readValue(exemploLinhasJson,Linhas.class);
			System.out.println(listLinhas.size() +"\n"+ listLinhas.get(0));
			 linhasService.save(listLinhas.get(0));
			// System.out.println(linhasService.getLinhasById("114633"));
		} catch (IOException e) {
			System.out.println(e.getMessage());
			System.out.println(e.getStackTrace().toString());
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}
	public static String distanciaCirculo(String latCentro, String lngCentro , double radius, String latPontoBusqueda, String lngPontoBusqueda) {
		String no="está no círculo";
		String fora="está fora do círculo";
		String dentro="está dentro do círculo";
		double y1 = Double.valueOf(latPontoBusqueda);
	    double x1 = Double.valueOf(lngPontoBusqueda);
//		double y1 = Double.valueOf("-30.00868779817800000");
//	    double x1 = Double.valueOf("-51.14298528234100000");
//	    https://www.google.com/maps/@-30.00868779817800000,-51.14298528234100000,21z

	    
	    //  circle is centered at 0,0
	    double y2 = Double.valueOf(latCentro);
	    double x2 = Double.valueOf(lngCentro);
//	    double y2 = Double.valueOf("-30.05213479817800000");
//	    double x2 = Double.valueOf("-51.22836428234100000");
//	    https://www.google.com/maps/@-30.05213479817800000,-51.22836428234100000,21z

	    radius = radius/100;
//	    System.out.println(Double.valueOf(radius).toString() );

	    double distance;
	    /**
	     * formula de distancia
	     *  d = √ ( x2 - x1 )^2 + (y2 - y1 )^2
	     */

	    distance = Math.pow( x2 - x1,2) + Math.pow(y2-y1,2);
	    distance = Math.sqrt(distance);

	    String result="";

	    if(distance < radius) {
	        result = dentro;
	    }
	    else if(distance > radius) {
	        result = fora;
	    }
	    else if(distance == radius) {
	        result =no;
	    }

	    return result;
	}

		
}
