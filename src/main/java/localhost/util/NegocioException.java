package localhost.util;

public class NegocioException extends Exception {
    private int errCode;

    public NegocioException(int errCode, String message) {
        super(message);
        this.errCode = errCode;
    }

    public int getErrCode() {
        return errCode;
    }

    public void setErrCode(int errCode) {
        this.errCode = errCode;
    }

	public NegocioException(int errCode, Throwable cause) {
		super(cause);
		this.errCode = errCode;
	}
	
	public NegocioException(int errCode, String message, Throwable cause) {
		super(message, cause);
		this.errCode = errCode;
	}

	public NegocioException(int errCode, String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		this.errCode = errCode;
	}

}
