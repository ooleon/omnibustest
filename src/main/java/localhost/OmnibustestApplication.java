package localhost;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import localhost.entity.Linhas;
import localhost.injection.LinhasService;
import localhost.repository.LinhasRepository;
import localhost.util.LinhasParser;

@SpringBootApplication
public class OmnibustestApplication {
	@Autowired
	LinhasService linhasService;

	public static void main(String[] args) {
		SpringApplication.run(OmnibustestApplication.class, args);
	}

	@Bean
	public CommandLineRunner setup(LinhasRepository linhasRepository) {
		return (args) -> {
			System.out.println("Command Line Runner");
/*			
			Linhas linhas = new Linhas();
//			linhas.setId( Long.valueOf(6) );
			linhas.setCodigo("ind6"); linhas.setNome("ind 6");
			linhasService.save(linhas);
			
			Linhas linhas1 = new Linhas();
//			linhas1.setId( Long.valueOf(-19) );
			linhas1.setCodigo("ind-19"); linhas1.setNome("ind -19");
			linhasService.save(linhas1);

			Linhas linhas2 = new Linhas();
//			linhas2.setId( Long.valueOf(-27) );
			linhas2.setCodigo("ind-27"); linhas2.setNome("ind -27");
			linhasService.save(linhas2);

			System.out.println("CommandLineRunner");
			LinhasParser lp = new LinhasParser();
			System.out.println("CommandLineRunner LinhasParser");
			lp.getLinhasParse();
*/			
		};
	}
}
