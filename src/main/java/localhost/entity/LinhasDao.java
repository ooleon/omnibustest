package localhost.entity;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;


public interface LinhasDao  extends JpaRepository<Linhas, String> {

	Linhas findByCodigoIgnoreCase(String paramString);

	List<Linhas> findByNomeIgnoreCase(String paramString);


}





