package localhost.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;


//@AllArgsConstructor
@Data 
@Entity
@Table(name="LINHA", schema="PUBLIC")
public class Linhas {
//	http://www.poatransporte.com.br/php/facades/process.php?a=nc&p=%&t=o
//    @GeneratedValue( strategy = GenerationType.AUTO, generator="PUBLIC.SYSTEM_SEQUENCE_FC0DF5C9_7BAD_4732_82E7_1CA97599DE85")

    @Id
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator="PUBLIC.SYSTEM_SEQUENCE_FC0DF5C9_7BAD_4732_82E7_1CA97599DE85")
    private Long id;
	private String codigo;
	private String nome;
	
	
	public Linhas() {}
	
	public Linhas(Long id, String codigo, String nome) {
		super();
		this.id = id;
		this.codigo = codigo;
		this.nome = nome;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	@Override
	public String toString() {
		return "{id=" + id + ", codigo=" + codigo + ", nome=" + nome + "}";
	}
	
}
