package localhost.injection;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import localhost.entity.Linhas;
import localhost.entity.LinhasDao;
import localhost.repository.LinhasRepository;
import localhost.util.AppConst;

import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;


@Service
public class LinhasService {
	private static final Logger logger = LogManager.getLogger(LinhasService.class);

	@Autowired
	private LinhasRepository linhasRepository;

	@Autowired
	private LinhasDao linhasDao;
	
	

	public List<Linhas> getAllLinhas() {
		return (List<Linhas>) this.linhasRepository.findAll();
	}

	public Linhas getLinhasById(String id) {
		return (Linhas) this.linhasRepository.findById( Long.valueOf(id) ).get();
	}
	public Linhas getLinhasById(long id) {
		return (Linhas) this.linhasRepository.findById( Long.valueOf(id) ).get();
	}
	public List<Linhas> getLinhasByNome(String nome) {
		// TODO Auto-generated method stub
		return (List<Linhas>) this.linhasDao.findByNomeIgnoreCase( nome );
	}

	public List<Linhas> getLinhasByCode(String Codigo) {
		// TODO Auto-generated method stub
		return (List<Linhas>) this.linhasDao.findByCodigoIgnoreCase( Codigo );
	}

	
	public String agregarLinhaBdLista(List<Linhas> listaLinhas) {
		for (Linhas linhas : listaLinhas) {
			this.save(linhas);
		}		
		logger.info(AppConst.LINHA_ATUALIZADA);
		return AppConst.LINHA_ATUALIZADA;
	}

	public String apagarBD() {
		List<Linhas> ll = this.linhasRepository.findAll();
		System.out.println("LL Inicial: " + ll.size());
		int i =0;

		this.removeAll();
		logger.info(AppConst.LINHA_APAGADA);
		return AppConst.LINHA_APAGADA;
	}
//	for (Linhas linhas : ll) {
//	this.remove(linhas);
//}
	
	public String getMessage() {
		logger.info(getMessage());
		return "(" + hashCode() + "): LinhasService!!!";
	}
	public Linhas save(Linhas l) {
		Linhas linhas = this.linhasRepository.save(l);
		logger.info(linhas.toString());
		return linhas;
	}
	
	public void remove(Linhas l) {
		this.linhasRepository.delete(l);
		logger.info(l.toString());
	}

	public void removeAll() {
		this.linhasRepository.deleteAll();
		logger.info("Remover todas as linhas");
	}
}
