package localhost.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(BlockJUnit4ClassRunner.class)
//@RunWith(SpringRunner.class)
//@SpringBootTest
public class CirculoTest {
	@Test
	public void distanciaDentroCirculoTest() {
		String resultado;

		resultado = LinhasParser.distanciaCirculo("-30.05213479817800000", "-51.22836428234100000", 18.579778416017447,
				"-30.00868779817800000", "-51.14298528234100000");
		System.out.println(resultado);
		assertEquals("está dentro do círculo", resultado);
	}

	@Test
	public void distanciaCirculoForaTest() {
		String resultado;

		resultado = LinhasParser.distanciaCirculo("-30.05213479817800000", "-51.22836428234100000", 8.579778416017447,
				"-30.00868779817800000", "-51.14298528234100000");
		System.out.println(resultado);
		assertEquals("está fora do círculo", resultado);
	}

	@Test
	public void distanciaNoCirculoTest() {
		String resultado;

		resultado = LinhasParser.distanciaCirculo("-30.05213479817800000", "-51.22836428234100000", 9.579778416017447,
				"-30.00868779817800000", "-51.14298528234100000");
		System.out.println(resultado);
		assertEquals("está no círculo", resultado);
	}


}
