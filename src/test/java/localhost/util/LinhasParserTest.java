package localhost.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.NumberFormat;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.junit.Assert;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import static org.hamcrest.CoreMatchers.any;
import static org.hamcrest.CoreMatchers.isA;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import localhost.entity.Linhas;
import localhost.entity.LinhasDao;
import localhost.injection.LinhasService;
import localhost.repository.LinhasRepository;
import localhost.restcontroller.AdminRestController;
import localhost.util.LinhasParser;

import org.springframework.boot.json.*;

//@RunWith(BlockJUnit4ClassRunner.class)
 @RunWith(SpringRunner.class)
 @SpringBootTest
public class LinhasParserTest {
	// @Autowired
	// private MockMvc mockMvc;

	private static final Logger logger = LogManager.getLogger(LinhasParserTest.class);

	@Autowired
	public AdminRestController adminRestController;
	@Autowired
	private LinhasService linhasService;

//	@MockBean
	private LinhasService mockLinhasService = mock(LinhasService.class, RETURNS_DEEP_STUBS);
//	@MockBean
	private LinhasRepository mockLinhasRepository;
//	@MockBean
	private LinhasDao mockLinhasDao;

	private List<Linhas> listLinhas;
	private InputStream inputStream = null;
	String urlLinhas = "http://www.poatransporte.com.br/php/facades/process.php?a=nc&p=%&t=l";
	String recursoJsonFile = "/json/linhas.json";

	String exemploLinhasJson = "{ \"id\":\"114633\", \"codigo\":\"60.21-2\", \"nome\":\"HOSPITAL CONCEICAO-IGUATEMI\"}";

	@Before
	public void setUpTest() {

		Linhas mocklinhas = new Linhas(1L, "mock", "mock");
		MockitoAnnotations.initMocks(this);
		// when(mockLinhasService.save( (Linhas) any(Linhas.class) )).thenReturn(
		// mocklinhas );
		// when(mockLinhasService.save((Linhas) any(Linhas.class) )).thenReturn(
		// ((Linhas) any(Linhas.class)) );
		// when(mockLinhasService.save( (Linhas) any(Linhas.class) )) ;

		doReturn(mocklinhas).when(mockLinhasService).save(mocklinhas);
	}

	@Test
	public void crudGuardarMock() {
		Linhas linhas = new Linhas();
		linhas.setId(Long.valueOf(6));
		linhas.setCodigo("ind6");
		linhas.setNome("ind 6");
		Linhas l = mockLinhasService.save(linhas);
		logger.log(Level.INFO, linhas);
	}

	public void listJsonLinhas() {
		// bien
		RestTemplate restTemplate = new RestTemplate();
		String resp = restTemplate.getForObject(urlLinhas, String.class);
		JsonParser springParser = JsonParserFactory.getJsonParser();
		// JsonParser springParser = new BasicJsonParser();
		List<Object> list = springParser.parseList(resp);
		List<Linhas> listlnh = new ArrayList<Linhas>();

		for (Object o : list) {
			if (o instanceof Map) {
				Map<String, Object> map = (Map<String, Object>) o;
				System.out.println("Items found: " + map.size());
				Linhas l = new Linhas();
				l.setId(Long.valueOf(map.get("id").toString()));
				l.setCodigo(map.get("codigo").toString());
				l.setNome(map.get("nome").toString());

				listlnh.add(l);

				/*
				 * int i = 0; for (Map.Entry<String, Object> entry : map.entrySet()) {
				 * System.out.println(entry.getKey() + " = " + entry.getValue()); i++; }
				 */

			}
		}
		System.out.println(listlnh);
	}

	public void listJsonObj() {
		// bien

		String url = "http://www.poatransporte.com.br/php/facades/process.php?a=nc&p=%&t=l";
		RestTemplate restTemplate = new RestTemplate();
		String resp = restTemplate.getForObject(url, String.class);

		JsonParser springParser = JsonParserFactory.getJsonParser();

		List<Object> list = springParser.parseList(resp);

		for (Object o : list) {
			if (o instanceof Map) {
				Map<String, Object> map = (Map<String, Object>) o;
				System.out.println("Items found: " + map.size());

				int i = 0;
				for (Map.Entry<String, Object> entry : map.entrySet()) {
					System.out.println(entry.getKey() + " = " + entry.getValue());
					i++;
				}

			}
		}
	}


	//@Test
	public void getLinhasWeb() {
		
		// System.out.println("urlLinhas:" + environment.getProperty("urlLinhas"));
//		ObjectMapper om = new ObjectMapper();
//		LinhasParser lp = new LinhasParser();
//		System.out.println("LinhasParserTest");
//		lp.getLinhasParse();
	}

}
