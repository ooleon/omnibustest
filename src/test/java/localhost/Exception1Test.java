package localhost;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import localhost.util.NegocioException;
import static junit.framework.TestCase.fail;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(BlockJUnit4ClassRunner.class)
public class Exception1Test {
    @Test(expected = ArithmeticException.class)
    public void testDivisionPorCeroException() {
        int j = 1 / 0;
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testListaVazia() {
        new ArrayList<>().get(0);
    }
        
}
