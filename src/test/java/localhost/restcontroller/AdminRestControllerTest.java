package localhost.restcontroller;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import localhost.entity.Linhas;
import localhost.injection.LinhasService;
import localhost.util.AppConst;
import localhost.util.NegocioException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AdminRestControllerTest {
	private static final Logger logger = LogManager.getLogger(AdminRestController.class);

	@Autowired
	public AdminRestController adminRestController;
	@Autowired
	private LinhasService linhasService;
	List<Linhas> ll;
	
	@Test
	public void apagarBdTest() {
		
		assertTrue(	linhasService.apagarBD().contains(AppConst.LINHA_APAGADA) );
	}
	@Test
	public void importBdTest() {
	assertTrue(	adminRestController.importarLinhasByUrl().getBody().contains(AppConst.BD_IMPORTADA) );
	}
	
	public void agregarJsonBD(List<Linhas> listaLinhas) {
		logger.info("LLsize2F: " + listaLinhas.size());
		for (Linhas linhas : listaLinhas) {
			adminRestController.importarLinhasByUrl();
//			adminRestController.guardar(linhas);
			logger.info("LLsize2F: " + listaLinhas.size() + " LinhaAgregada: " + linhas);
		}		
	}

}
