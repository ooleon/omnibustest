Feature: Crear Linhas

  Scenario: registro satisfatorio com tudos os campos requeridos.

    Given usuario quere crear uma linha nova com os seguintes atribudos
      | id  | codigo | nome |
      | 100 | TA1    | Transversar Adicional 1    |

    When usuario guarda a linha nova 'COM TODOS OS CAMPOS PRENCHIDOS'
    Then O salve 'foi feito'


  Scenario Outline: <testCase> <expectedResult>

    Given user wants to create an employee with the following attributes
      | id  | codigo   | nome   |
      | 110 | <codigo> | <nome> |

    When user saves the new employee '<testCase>'
    Then the save '<expectedResult>'

    Examples:
      | testCase                 | expectedResult | codigo | nome                    |
      | SEM CODIGO               | FAILS          |        | Transversar Adicional 2 |
      | SEM NOME                 | FAILS          | TA2    |                         |
      | WITH ALL REQUIRED FIELDS | foi feito      | TA2    | Transversar Adicional 2 |

