Feature: Is it Friday yet?
  Everybody wants to know when it's Friday

  Scenario Outline: Today is or is not Friday
     
    #Dado today is "<day>"
    #Onde I ask whether it's Friday yet
    #Entao I should be told "<answer>"
    Given today is "<day>"
    And algo mais
    When I ask whether it's Friday yet
    Then I should be told "<answer>"
    
  Examples:
    | day            | answer |
    | Friday         | TGIF   |
    | Sunday         | Nope   |
    | anything else! | Nope   |
        

